package at.agnet.demo_consumer;

import io.github.majusko.pulsar.producer.PulsarTemplate;
import org.apache.pulsar.client.api.PulsarClientException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ContactConsumer {

    @Autowired
    private PulsarTemplate<Contact> producer;


    public void sendContact(Contact msg) throws PulsarClientException {
        var c = new Contact(msg.firstName() + " new", msg.lastName(), msg.address());
        this.producer.send("read-contact-topic", c);
    }
}
