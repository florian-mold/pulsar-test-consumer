package at.agnet.demo_consumer;

public record Contact(String firstName, String lastName, String address) {
}
