package at.agnet.demo_consumer;

import io.github.majusko.pulsar.annotation.PulsarConsumer;
import io.github.majusko.pulsar.producer.PulsarTemplate;
import org.apache.pulsar.client.api.PulsarClientException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ContactProducer {

    @Autowired
    private PulsarTemplate<Contact> producer;

    public void sendHelloWorld() throws PulsarClientException {
        producer.send("create-contact-topic", new Contact("Florian", "Mold", "Wien"));
    }

    @PulsarConsumer(topic = "read-contact-topic", clazz = Contact.class)
    void consume(Contact msg) {
        System.out.println("ContactProducer");
        System.out.println(msg);
    }
}
